export default function parseAttrs(attrString){
    if(!attrString){
        return []
    }
    // 这里要判断遇到的两种空格，一种是引号内的空格 一种是引号外的
    // 这里加入一个是否是引号内空格的判断
    // 如果遇到在引号外的空格，就将指针到当前索引的字符串切割push到数组中

    let bool = false
    let point = 0
    let arr = []
    for(let i = 0; i < attrString.length; i++){
        let char = attrString[i]
        if( char === '"'){
            bool = !bool
        } else if(char === ' ' && !bool){
            const subStr = attrString.substring(point,i)
            if(!/^\s*$/.test(subStr)){
                arr.push(subStr.trim())
                point = i
            }
        }
    }
    arr.push(attrString.substring(point).trim())

    arr = arr.map(item=>{
        const data = item.split('=')
        const name = data[0]
        const value = data[1].replace(/"/g,'')
        return {name,value}
    })
    return arr
}