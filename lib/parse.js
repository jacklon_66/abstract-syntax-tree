import parseAttrs from "./parseAttrs";
export default function parse(tempStr){
    let index = 0;
    let tail = ''
    const startReg = /^<([a-z]+[1-6]?)(\s[^<]+)?>/
    const endReg = /^<\/([a-z]+[1-6]?)>/
    const wordRegExp = /^([^<]+)<\/(\w+)>/
    const tagStack = []
    const contentStack = []
    
    while(index < tempStr.length-1){
        tail = tempStr.substring(index)
        if(startReg.test(tail)){
            const matchRes = tail.match(startReg)
            // 标签字符串
            const startTag = matchRes[1]
            // 属性字符串
            const attrString = matchRes[2]
            // 开始将标记放入到栈1中
            tagStack.push(startTag)
            // 将对象推入数组
            contentStack.push({tag:startTag, children:[], attrs: parseAttrs(attrString)})
            // 得到attrString的长度
            const attrsStringLength = attrString ? attrString.length : 0
            // 指针移动标签的长度 + 2 + attrsString.length
            // 为什么 +2,因为 <> 也占两个长度
            index += startTag.length + attrsStringLength + 2
        }else if(endReg.test(tail)){
            const endTag = tail.match(endReg)[1]
            
            if(endTag === tagStack[tagStack.length-1]){
                if(tagStack.length === 1 && contentStack.length === 1){
                    break
                }
                tagStack.pop()
                const pop_content = contentStack.pop()
                contentStack[contentStack.length-1].children.push(pop_content)
                // 指针移动标签的长度 + 3,为什么 +3,因为 </> 也占三个长度
                index += endTag.length + 3   
            }else{
                throw Error('标签未闭合')
            }
        }else if(wordRegExp.test(tail)){
            // 识别遍历到的这个字符，是不是文字，并且不能全是空字符
            let word = tail.match(wordRegExp)[1]
            if(!/^\s+$/.test(word)){
                contentStack[contentStack.length-1].children.push({text:word})
            }
            index += word.length
        }else{
            index++
        }
    }

    return contentStack[0]
}