const arr = [[1,2,3,[4,5,6,[7,8,9]]]]
const data = [{
    children:[
        {value:1},
        {value:2},
        {value:3},
        {
            children:[
                {value:4},
                {value:5},
                {value:6},
                {
                    children:[
                        {value:7},
                        {value:8},
                        {value:9},
                    ]
                }
            ]
        }
    ]
}]

// function convert(list){
//     const result = []
//     for(let i of list){
//         if(typeof i === 'number'){
//             result.push({value:i})
//         }else{
//             result.push({
//                 children:convert(i)
//             })
//         }
//     }
//     return result
// }

function convert(item){
    if(typeof item === 'number'){
        return {
            value:item
        }
    }else if(Array.isArray(item)){
        return {
            children:item.map(_item => convert(_item))
        }
    }
}
let res  = convert(arr)

console.log('res: ',JSON.stringify(res));
// console.table(data)