// 在一个长字符串中找出重复次数最多字符

function lookupMaxRepeatChar(str){
    let a = 0
    let b = 0
    let maxRepeatCount = 0
    let maxRepeatChar = ''
    while(a<=str.length-1){
        if(str[a]!==str[b]){
           if(b-a>maxRepeatCount){
               maxRepeatChar = str[a]
               maxRepeatCount = b - a
           } 
           a = b
        }
        b++
    }
    return {maxRepeatCount,maxRepeatChar}
}

module.exports.lookupMaxRepeatChar = lookupMaxRepeatChar