function smartRepeat(templateStr){
    let index = 0
    const numStack = []
    const strStack = []
    let tail = templateStr
    while(index < templateStr.length-1){
        tail = templateStr.substring(index)
        if(/^\d+\[/.test(tail)){
            const times = Number(tail.match(/^(\d)+\[/)[1])
            numStack.push(times)
            strStack.push('')
            index += times.toString().length + 1
        }else if(/^\w+\]/.test(tail)){
            const word = tail.match(/^(\w+)\]/)[1]
            strStack[strStack.length-1] = word
            index += word.length
        }else if(tail[0] === ']'){
            const times = numStack.pop()
            const word = strStack.pop()
            strStack[strStack.length-1] += word.repeat(times)
            index++
        }else{
            index++
        }
    }
    // console.log(numStack,strStack);
    return strStack[0].repeat(numStack[0])
}
const res = smartRepeat('3[2[ab]3[bc]]')
console.log('res: ', res);
