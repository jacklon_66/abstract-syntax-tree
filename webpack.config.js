const path = require('path')
module.exports = {
    mode:'development',
    entry:'./index.js',
    output:{
        filename:'bundle.js'
    },
    devServer:{
        contentBase:path.join(__dirname,'www'),
        compress:false,
        port:9527,
        publicPath:'/xuni/'
    }
}