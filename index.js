import parse from "./lib/parse";

const template = `<div class="aa bb cc" id="app">
  <h3 class="title">hello world</h3>
  <p>this page is abstract syntax tree</p>
  <ul>
    <li>123</li>
    <li>456</li>
    <li>789</li>
  </ul>
</div>`

const result = parse(template)
console.log(result);